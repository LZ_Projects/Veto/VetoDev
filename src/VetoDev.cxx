#include "VetoDev.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsVetoDev.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Constructor
VetoDev::VetoDev() : Analysis() {
  // Root file branches required
  m_event->IncludeBranch("pulsesTPC");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("eventHeader");

  m_event->Initialize();

  // Setup logging
  logging::set_program_name("VetoDev Analysis");

  // Setup the analysis specific cuts.
  m_cutsVetoDev = new CutsVetoDev(m_event);

  // Setup config service so it can be used to get config variables
  m_conf = ConfigSvc::Instance();
}

// Destructor
VetoDev::~VetoDev() { delete m_cutsVetoDev; }

// Call before event loop
void VetoDev::Initialize() { INFO("Initializing VetoDev Analysis"); }

// Called once per event
void VetoDev::Execute() {

  // Setup etrain
  // unsigned long long dT{m_cutsVetoDev->ETrainSetup()};

  // Select OD Pulses
  std::vector<int> odPulses{ODGetAllPulseIDs()}; // All Pulses
  // std::vector<int> odPulses_cut0{ODGetCutPulseIDs(odPulses, 0)};
  // std::vector<int> odPulses_cut1{ODGetCutPulseIDs(odPulses, 1)};
  // std::vector<int> odPulses_cut2{ODGetCutPulseIDs(odPulses, 2)};
  std::vector<int> odPulses_cut3{ODGetCutPulseIDs(odPulses, 3)};

  g_ODEventArea = ODGetEventPhd(odPulses); // OD Event Area

  // Start studies - Process without TPC cut
  // Leading Vs Subleading Study
  // ODLeadingVsSubleadingStudy("AllPulses", odPulses);
  // ODLeadingVsSubleadingStudy("ODPulseCut0", odPulses_cut0);
  // ODLeadingVsSubleadingStudy("ODPulseCut1", odPulses_cut1);
  // ODLeadingVsSubleadingStudy("ODPulseCut2", odPulses_cut2);
  ODLeadingVsSubleadingStudy("ODPulseCut3", odPulses_cut3);

  // Process without TPC event
  // ODProcessODPulses("NoTPCCut/AllPulses", odPulses, false);
  // ODProcessODPulses("NoTPCCut/CutPulses", odPulses_Cut, false);
  // ODProcessODPulses("NoTPCCut/LeadingPulse", odLeadingPulse, false);
  // ODProcessODPulses("NoTPCCut/LeadingAndSubleading",
  // odLeadingAndSubleadingPulses, false); ODLeadingVsSubLeading("AllPulses",
  // odLeadingAndSubleadingPulses_AllPulses);

  // Process if a SS event
  // if (!(*m_event->m_singleScatter)->nSingleScatters) return;
  // ApplyMDC3Cuts("AllPulses", odPulses, true);
  // ApplyMDC3Cuts("CutPulses", odPulses_Cut, false);
  // ApplyMDC3Cuts("LeadingPulse", odLeadingPulse, true);
  // ApplyMDC3Cuts("LeadingAndSubleadingPulses", odLeadingAndSubleadingPulses);
  // ODLeadingVsSubLeading("TPCCut", odLeadingAndSubleadingPulses_CutPulses);

  // Sort etrain for next event
  // m_cutsVetoDev->ETrainEndOfEvent(dT);
}

// Called after event loop
void VetoDev::Finalize() {
  INFO("Finalizing VetoDev Analysis");
  // TODO Move axis labels here, should speed up execution
}

// Pulse Processing
void VetoDev::ODProcessODPulses(std::string path, std::vector<int> pulseIDs,
                                bool processSS) {

  // Process with no channels dead
  ODPulseClassificationHistograms(path, pulseIDs);

  // If process SS
  if (processSS) {
    ODPulsesRelativeToSS(path, pulseIDs);
  }
}

void VetoDev::ApplyMDC3Cuts(std::string path, std::vector<int> odpulseIDs) {

  std::string efficiencyHist = "Efficiency_NEvents";
  /*
  // TODO Add etrain cut
  // TODO Readd Dead PMT Study code
  bool hasPulses{false};
  if (odpulseIDs.size() > 0)
    hasPulses = true;

  if (hasPulses) {
    // Cut0 just SS
    //ODProcessODPulses("MDC3Cuts/Cut0_SS/" + path, odpulseIDs, true);
    ODLeadingVsSubLeading("MDC3Cuts/Cut0_SS/" + path, odpulseIDs);
    int s1_ID{(*m_event->m_singleScatter)->s1PulseID};
    // ROI
    if (m_cutsVetoDev->WIMP_ROI_Veto(s1_ID)) {
      //ODProcessODPulses("MDC3Cuts/Cut1_ROI/" + path, odpulseIDs, true);
      ODLeadingVsSubLeading("MDC3Cuts/Cut1_ROI/" + path, odpulseIDs);
      // FID
      if (m_cutsVetoDev->UncorrectedFiducialVeto()) {
        //ODProcessODPulses("MDC3Cuts/Cut2_FID/" + path, odpulseIDs, true);
        ODLeadingVsSubLeading("MDC3Cuts/Cut2_FID/" + path, odpulseIDs);
        // Skin
        if (m_cutsVetoDev->SkinVeto(s1_ID)) {
            //ODProcessODPulses("MDC3Cuts/Cut3_Skin/" + path, odpulseIDs, true);
            ODLeadingVsSubLeading("MDC3Cuts/Cut3_Skin/" + path, odpulseIDs);
            // OD All Channels
            if (m_cutsVetoDev->ODVeto(s1_ID, odpulseIDs)) {
              //ODProcessODPulses("MDC3Cuts/Cut4_OD/" + path, odpulseIDs, true);
              ODLeadingVsSubLeading("MDC3Cuts/Cut4_OD/" + path, odpulseIDs);
            }
        }
      }
    }
  }
   */
}

// Pulse Selection
std::vector<int> VetoDev::ODGetAllPulseIDs() {
  std::vector<int> odPulses;
  for (int i = 0; i < (*m_event->m_odHGPulses)->nPulses; ++i) {
    odPulses.push_back(i);
  }
  return odPulses;
}

std::vector<int> VetoDev::ODGetCutPulseIDs(std::vector<int> odPulses,
                                           int cutlevel) {
  std::vector<int> survivingPulses;
  int coincidence{0};
  float phd{0.0};
  for (auto &&pulseID : odPulses) {
    coincidence = (*m_event->m_odHGPulses)->coincidence[pulseID];
    phd = (*m_event->m_odHGPulses)->pulseArea_phd[pulseID];
    if (m_cutsVetoDev->ODPulseCut(coincidence, phd, cutlevel)) {
      survivingPulses.push_back(pulseID);
    }
  }
  return survivingPulses;
}

std::vector<int> VetoDev::ODGetLeadingPulseID(std::vector<int> odPulses) {
  int pulseID{-1};
  float maxArea{0.0};
  float currentArea{0.0};
  std::vector<int> survivingPulses;
  for (auto &&i : odPulses) {
    currentArea = (*m_event->m_odHGPulses)->pulseArea_phd[i];
    if (currentArea > maxArea) {
      maxArea = currentArea;
      pulseID = i;
    }
  }
  if (pulseID > -1) {
    survivingPulses.push_back(pulseID);
  }
  return survivingPulses;
}

std::vector<int>
VetoDev::ODGetLeadingAndSubLeadingPulses(std::vector<int> odPulses) {

  std::vector<int> survivingPulses;
  if (odPulses.size() < 2) {
    return survivingPulses;
  } else {
    int leadingID{-1};
    int subleadingID{-1};
    float leadingArea{0.0};
    float subleadingArea{0.0};
    float currentArea{0.0};
    // Get Leading
    for (auto &&i : odPulses) {
      currentArea = (*m_event->m_odHGPulses)->pulseArea_phd[i];
      if (currentArea > leadingArea) {
        leadingArea = currentArea;
        leadingID = i;
      }
    }
    // Subleading
    for (auto &&i : odPulses) {
      currentArea = (*m_event->m_odHGPulses)->pulseArea_phd[i];
      if (currentArea > subleadingArea) {
        if (i != leadingID) {
          subleadingArea = currentArea;
          subleadingID = i;
        }
      }
    }
    survivingPulses.push_back(leadingID);
    survivingPulses.push_back(subleadingID);
    return survivingPulses;
  }
}

float VetoDev::ODGetEventPhd(std::vector<int> pulseIDs) {
  float totalphd{0.0};
  for (auto &&i : pulseIDs) {
    totalphd += (*m_event->m_odHGPulses)->pulseArea_phd[i];
  }
  return totalphd;
}

void VetoDev::ODPulseClassificationHistograms(std::string path,
                                              std::vector<int> pulseIDs) {
  // Ignore if no pulse IDs
  if (pulseIDs.size() < 1)
    return;

  // MultiUse Variables
  std::string histpath;
  float pulseArea{0.0};
  int coincidence{0};
  float aft5{0.0};
  float aft95{0.0};
  float pulseWidth90{0.0};
  float pulseHeight{0.0};
  float pF50{0.0};
  float pF200{0.0};
  float pulseWidthRMS{0.0};
  float pulseRiseTime{0.0};

  // Variables for ignoring channels
  int count{0};
  std::vector<int>::iterator it;

  // Loop over Pulses
  for (auto &&pulseID : pulseIDs) {
    // Extract RQs for pulse
    coincidence = (*m_event->m_odHGPulses)->coincidence[pulseID];
    pulseArea = (*m_event->m_odHGPulses)->pulseArea_phd[pulseID];
    aft5 = (*m_event->m_odHGPulses)->areaFractionTime5_ns[pulseID];
    aft95 = (*m_event->m_odHGPulses)->areaFractionTime95_ns[pulseID];
    pulseWidth90 = aft95 - aft5;
    pulseWidthRMS = (*m_event->m_odHGPulses)->rmsWidth_ns[pulseID];
    pulseRiseTime = (*m_event->m_odHGPulses)->peakTime_ns[pulseID];
    pulseHeight = (*m_event->m_odHGPulses)->peakAmp[pulseID];
    pF50 = (*m_event->m_odHGPulses)->promptFraction50ns[pulseID];
    pF200 = (*m_event->m_odHGPulses)->promptFraction200ns[pulseID];

    // Fill Histograms
    histpath = path + "/OD1D/Coincidence";
    m_hists->BookFillHist(histpath, 120., 0., 120., coincidence);
    TH1 *hist_coinc = m_hists->GetHistFromMap(histpath);
    hist_coinc->GetXaxis()->SetTitle("Coincidence");

    histpath = path + "/OD1D/PulseArea";
    m_hists->BookFillHist(histpath, 1500., 0., 1500., pulseArea);
    TH1 *hist_pa = m_hists->GetHistFromMap(histpath);
    hist_pa->GetXaxis()->SetTitle("Pulse Area (phd)");

    histpath = path + "/OD1D/AreaFractionTime_5";
    m_hists->BookFillHist(histpath, 100., 0., 1000., aft5);
    TH1 *hist_aft5 = m_hists->GetHistFromMap(histpath);
    hist_aft5->GetXaxis()->SetTitle("Time (ns)");

    histpath = path + "/OD1D/AreaFractionTime_95";
    m_hists->BookFillHist(histpath, 100., 0., 1000., aft95);
    TH1 *hist_aft95 = m_hists->GetHistFromMap(histpath);
    hist_aft95->GetXaxis()->SetTitle("Time (ns)");

    histpath = path + "/OD1D/PulseWidth90";
    m_hists->BookFillHist(histpath, 100., 0., 1000., pulseWidth90);
    TH1 *hist_pw90 = m_hists->GetHistFromMap(histpath);
    hist_pw90->GetXaxis()->SetTitle("Time (ns)");

    histpath = path + "/OD1D/PulseWidthRMS";
    m_hists->BookFillHist(histpath, 100., 0., 1000., pulseWidthRMS);
    TH1 *hist_pwRMS = m_hists->GetHistFromMap(histpath);
    hist_pwRMS->GetXaxis()->SetTitle("Time (ns)");

    histpath = path + "/OD1D/PulseHeight";
    m_hists->BookFillHist(histpath, 100., 0., 14., pulseHeight);
    TH1 *hist_ph = m_hists->GetHistFromMap(histpath);
    hist_ph->GetXaxis()->SetTitle("phd/sample");

    histpath = path + "/OD1D/PulseRiseTime";
    m_hists->BookFillHist(histpath, 60., 0., 600., pulseRiseTime);
    TH1 *hist_prt = m_hists->GetHistFromMap(histpath);
    hist_prt->GetXaxis()->SetTitle("Time (ns)");

    histpath = path + "/OD1D/PulseFraction50ns";
    m_hists->BookFillHist(histpath, 100., -6., 6., pF50);
    TH1 *hist_pf50 = m_hists->GetHistFromMap(histpath);
    hist_pf50->GetXaxis()->SetTitle("Fraction");

    histpath = path + "/OD1D/PulseFraction200ns";
    m_hists->BookFillHist(histpath, 28., -4., 10., pF200);
    TH1 *hist_pf200 = m_hists->GetHistFromMap(histpath);
    hist_pf200->GetXaxis()->SetTitle("Fraction");

    histpath = path + "/OD2D/Coincidence_Over_PulseArea";
    m_hists->BookFillHist(histpath, 1500., 0., 1500., 120., 0., 120., pulseArea,
                          coincidence);
    TH1 *hist_paci_2d = m_hists->GetHistFromMap(histpath);
    hist_paci_2d->GetXaxis()->SetTitle("Pulse Area (phd)");
    hist_paci_2d->GetYaxis()->SetTitle("Coincidence");

    histpath = path + "/OD1D/CoincidenceOverPulseArea";
    m_hists->BookFillHist(histpath, 6000., 0., 3000., coincidence / pulseArea);
    TH1 *hist_paci = m_hists->GetHistFromMap(histpath);
    hist_paci->GetXaxis()->SetTitle("Coincidence Over Pulse Area");
  }
}

void VetoDev::ODPulsesRelativeToSS(std::string path,
                                   std::vector<int> pulseIDs) {
  // Check if worth doing things
  if (pulseIDs.size() < 1)
    return;
  if (!(*m_event->m_singleScatter)->nSingleScatters)
    return;

  // Variables
  std::string histpath;
  // S1 Variables
  int s1_ID = (*m_event->m_singleScatter)->s1PulseID;
  float ss_s1tt = (*m_event->m_tpcPulses)->pulseStartTime_ns[s1_ID];
  float ss_s1t5p = (*m_event->m_tpcPulses)->areaFractionTime5_ns[s1_ID];
  float ss_xc = (*m_event->m_singleScatter)->correctedX_cm;
  float ss_yc = (*m_event->m_singleScatter)->correctedY_cm;
  float ss_s1c_phd = (*m_event->m_singleScatter)->correctedS1Area_phd;
  // OD Variables
  float od_tt;
  float od_t5p;
  float od_phd;
  int od_coincidence;
  float timedifference;
  float od_amp;

  // Loop over pulses
  for (auto &&i : pulseIDs) {
    // Set variables
    od_tt = (*m_event->m_odHGPulses)->pulseStartTime_ns[i];
    od_t5p = (*m_event->m_odHGPulses)->areaFractionTime5_ns[i];
    timedifference = od_tt + od_t5p - ss_s1tt - ss_s1t5p;
    od_phd = (*m_event->m_odHGPulses)->pulseArea_phd[i];
    od_coincidence = (*m_event->m_odHGPulses)->coincidence[i];
    od_amp = (*m_event->m_odHGPulses)->peakAmp[i];

    histpath = path + "/ODSS/s1_to_od_time_trigger_5percent_long";
    m_hists->BookFillHist(histpath, 20000., -1000000., 1000000.,
                          timedifference);
    TH1 *hist_tdlb = m_hists->GetHistFromMap(histpath);
    hist_tdlb->GetXaxis()->SetTitle("time difference (ns)");

    histpath = path + "/ODSS/s1_to_od_time_trigger_5percent_short";
    m_hists->BookFillHist(histpath, 600., -3000., 3000., timedifference);
    TH1 *hist_tdsb = m_hists->GetHistFromMap(histpath);
    hist_tdsb->GetXaxis()->SetTitle("time difference (ns)");

    histpath = path + "/ODSS/s1_area_vs_timedifference_long";
    m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 100., 0.,
                          1000., timedifference, ss_s1c_phd);
    TH1 *hist_s1vtdl = m_hists->GetHistFromMap(histpath);
    hist_s1vtdl->GetXaxis()->SetTitle("time difference (ns)");
    hist_s1vtdl->GetYaxis()->SetTitle("s1 phd (corrected)");

    histpath = path + "/ODSS/s1_area_vs_timedifference_short";
    m_hists->BookFillHist(histpath, 600., -3000., 3000., 100., 0., 1000.,
                          timedifference, ss_s1c_phd);
    TH1 *hist_s1vtds = m_hists->GetHistFromMap(histpath);
    hist_s1vtds->GetXaxis()->SetTitle("time difference (ns)");
    hist_s1vtds->GetYaxis()->SetTitle("s1 phd (corrected)");

    histpath = path + "/ODSS/OD_AreaOverHeight_vs_timedifference_long";
    m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 20., 0., 200.,
                          timedifference, od_phd / od_amp);
    TH1 *hist_paphl = m_hists->GetHistFromMap(histpath);
    hist_paphl->GetXaxis()->SetTitle("Time Difference (ns)");
    hist_paphl->GetYaxis()->SetTitle("Pulse Area Over Height");

    histpath = path + "/ODSS/OD_AreaOverHeight_vs_timedifference_short";
    m_hists->BookFillHist(histpath, 600., -3000., 3000., 20., 0., 200.,
                          timedifference, od_phd / od_amp);
    TH1 *hist_paphs = m_hists->GetHistFromMap(histpath);
    hist_paphs->GetXaxis()->SetTitle("Time Difference (ns)");
    hist_paphs->GetYaxis()->SetTitle("Pulse Area Over Height");

    histpath = path + "/ODSS/OD_PHD_vs_timedifference_long";
    m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 1500., 0.,
                          1500., timedifference, od_phd);
    TH1 *hist_odphdvtdl = m_hists->GetHistFromMap(histpath);
    hist_odphdvtdl->GetXaxis()->SetTitle("time difference (ns)");
    hist_odphdvtdl->GetYaxis()->SetTitle("phd");

    histpath = path + "/ODSS/OD_PHD_vs_timedifference_short";
    m_hists->BookFillHist(histpath, 600., -3000., 3000., 1500., 0., 1500.,
                          timedifference, od_phd);
    TH1 *hist_odphdvtds = m_hists->GetHistFromMap(histpath);
    hist_odphdvtds->GetXaxis()->SetTitle("time difference (ns)");
    hist_odphdvtds->GetYaxis()->SetTitle("phd");

    histpath = path + "/ODSS/OD_Coincidence_vs_timedifference_long";
    m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 120., 0., 120.,
                          timedifference, od_coincidence);
    TH1 *hist_cvtdl = m_hists->GetHistFromMap(histpath);
    hist_cvtdl->GetXaxis()->SetTitle("Time Difference (ns)");
    hist_cvtdl->GetYaxis()->SetTitle("Coincidence");

    histpath = path + "/ODSS/OD_Coincidence_vs_timedifference_short";
    m_hists->BookFillHist(histpath, 600., -3000., 3000., 120., 0., 120.,
                          timedifference, od_coincidence);
    TH1 *hist_cvtds = m_hists->GetHistFromMap(histpath);
    hist_cvtds->GetXaxis()->SetTitle("Time Difference (ns)");
    hist_cvtds->GetYaxis()->SetTitle("Coincidence");

    histpath = path + "/ODSS/CoincidenceOverPulseArea_vs_timedifference_short";
    m_hists->BookFillHist(histpath, 600., -3000, 3000, 1000., 0., 100.,
                          timedifference, od_coincidence / od_phd);
    TH1 *hist_paci = m_hists->GetHistFromMap(histpath);
    hist_paci->GetYaxis()->SetTitle("Coincidence Over Pulse Area");
    hist_paci->GetXaxis()->SetTitle("Time Difference (ns)");

    histpath = path + "/ODSS/CoincidenceOverPulseArea_vs_timedifference_long";
    m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 1000., 0.,
                          100., timedifference, od_coincidence / od_phd);
    TH1 *hist_paci_l = m_hists->GetHistFromMap(histpath);
    hist_paci_l->GetYaxis()->SetTitle("Coincidence Over Pulse Area");
    hist_paci_l->GetXaxis()->SetTitle("Time Difference (ns)");
  }
}

void VetoDev::ODLeadingVsSubleadingStudy(std::string histpath,
                                         std::vector<int> odPulses) {

  // Get Leading and Subleading Pulses for pulses above
  std::vector<int> two_pulses{ODGetLeadingAndSubLeadingPulses(odPulses)};

  // Perform analysis
  ODLeadingVsSubLeadingHistograms(histpath, two_pulses, odPulses.size(), true);
}

void VetoDev::ODLeadingVsSubLeadingHistograms(std::string path,
                                              std::vector<int> pulseIDs,
                                              int pulseMultiplicity,
                                              bool selectRegion) {

  // Dont do if no pulses...
  if (pulseIDs.size() < 1)
    return;

  // Setup Variables
  std::string histpath;
  // Leading
  int l_id{pulseIDs[0]};
  float l_pulseArea{(*m_event->m_odHGPulses)->pulseArea_phd[l_id]};
  int l_coincidence{(*m_event->m_odHGPulses)->coincidence[l_id]};
  float l_starttime{(*m_event->m_odHGPulses)->pulseStartTime_ns[l_id] +
                    (*m_event->m_odHGPulses)->areaFractionTime5_ns[l_id]};
  float l_height{(*m_event->m_odHGPulses)->peakAmp[l_id]};
  float l_width{(*m_event->m_odHGPulses)->areaFractionTime99_ns[l_id]};
  float l_h2w{l_height / l_width};
  // Subleading
  int s_id{pulseIDs[1]};
  float s_pulseArea{(*m_event->m_odHGPulses)->pulseArea_phd[s_id]};
  int s_coincidence{(*m_event->m_odHGPulses)->coincidence[s_id]};
  float s_starttime{(*m_event->m_odHGPulses)->pulseStartTime_ns[s_id] +
                    (*m_event->m_odHGPulses)->areaFractionTime5_ns[s_id]};
  float s_height{(*m_event->m_odHGPulses)->peakAmp[s_id]};
  float s_width{(*m_event->m_odHGPulses)->areaFractionTime99_ns[s_id]};
  float s_h2w{s_height / s_width};
  // Combined
  float od_timediff{s_starttime - l_starttime};
  float od_phdratio{s_pulseArea / l_pulseArea};
  float od_coincidenceratio{s_coincidence / l_coincidence};
  float od_h2wratio{s_h2w / l_h2w};

  /* ********** \\
  // Histograms \\
  ** ********** */

  // Leading Pulse
  histpath = path + "/ODLS/LeadingPulse/PulseArea";
  m_hists->BookFillHist(histpath, 1500., 0., 1500., l_pulseArea);
  TH1 *hist_leading_phd = m_hists->GetHistFromMap(histpath);
  hist_leading_phd->GetXaxis()->SetTitle("phd");

  histpath = path + "/ODLS/LeadingPulse/PulseArea_vs_EventArea";
  m_hists->BookFillHist(histpath, 1500., 0., 1500., 1500., 0., 1500.,
                        g_ODEventArea, l_pulseArea);
  TH1 *hist_leading_phd_vs_ea = m_hists->GetHistFromMap(histpath);
  hist_leading_phd_vs_ea->GetXaxis()->SetTitle("event phd");
  hist_leading_phd_vs_ea->GetYaxis()->SetTitle("leading phd");

  histpath = path + "/ODLS/LeadingPulse/PulseArea_vs_EventArea_difference";
  m_hists->BookFillHist(histpath, 1500., 0., 1500.,
                        g_ODEventArea - l_pulseArea);
  TH1 *hist_leading_phd_vs_ea_diff = m_hists->GetHistFromMap(histpath);
  hist_leading_phd_vs_ea_diff->GetXaxis()->SetTitle("phd difference");

  histpath = path + "/ODLS/LeadingPulse/PulseArea_vs_EventArea_percentage";
  m_hists->BookFillHist(histpath, 200., 0., 100.,
                        (l_pulseArea / g_ODEventArea) * 100);
  TH1 *hist_leading_phd_vs_ea_p = m_hists->GetHistFromMap(histpath);
  hist_leading_phd_vs_ea_p->GetXaxis()->SetTitle("percentage");

  histpath = path + "/ODLS/LeadingPulse/Coincidence";
  m_hists->BookFillHist(histpath, 120., 0., 120., l_coincidence);
  TH1 *hist_leading_coincidence = m_hists->GetHistFromMap(histpath);
  hist_leading_coincidence->GetXaxis()->SetTitle("leading coincidence");

  histpath = path + "/ODLS/LeadingPulse/H2W";
  m_hists->BookFillHist(histpath, 100., 0., 0.1, l_h2w);
  TH1 *hist_leading_h2w = m_hists->GetHistFromMap(histpath);
  hist_leading_h2w->GetXaxis()->SetTitle("h2w");

  histpath = path + "/ODLS/LeadingPulse/PulseArea_vs_H2W";
  m_hists->BookFillHist(histpath, 100., 0., 0.1, 1500., 0., 1500., l_h2w,
                        l_pulseArea);
  TH1 *hist_leading_phd_vs_h2w = m_hists->GetHistFromMap(histpath);
  hist_leading_phd_vs_h2w->GetXaxis()->SetTitle("h2w");
  hist_leading_phd_vs_h2w->GetYaxis()->SetTitle("phd");

  histpath = path + "/ODLS/LeadingPulse/PulseArea_vs_TimeDifference_short";
  m_hists->BookFillHist(histpath, 600., -3000., 3000., 1500., 0., 1500.,
                        od_timediff, l_pulseArea);
  TH1 *hist_leading_phd_vs_tds = m_hists->GetHistFromMap(histpath);
  hist_leading_phd_vs_tds->GetXaxis()->SetTitle("time (ns)");
  hist_leading_phd_vs_tds->GetYaxis()->SetTitle("phd");

  histpath = path + "/ODLS/LeadingPulse/PulseArea_vs_TimeDifference_long";
  m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 1500., 0., 1500.,
                        od_timediff, l_pulseArea);
  TH1 *hist_leading_phd_vs_tdl = m_hists->GetHistFromMap(histpath);
  hist_leading_phd_vs_tdl->GetXaxis()->SetTitle("time (ns)");
  hist_leading_phd_vs_tdl->GetYaxis()->SetTitle("phd");

  histpath = path + "/ODLS/LeadingPulse/Coincidence_vs_TimeDifference_short";
  m_hists->BookFillHist(histpath, 600., -3000., 3000., 120., 0., 120.,
                        od_timediff, l_coincidence);
  TH1 *hist_leading_coincidence_vs_tds = m_hists->GetHistFromMap(histpath);
  hist_leading_coincidence_vs_tds->GetXaxis()->SetTitle("time (ns)");
  hist_leading_coincidence_vs_tds->GetYaxis()->SetTitle("coincidence");

  histpath = path + "/ODLS/LeadingPulse/Coincidence_vs_TimeDifference_long";
  m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 120., 0., 120.,
                        od_timediff, l_coincidence);
  TH1 *hist_leading_coincidence_vs_tdl = m_hists->GetHistFromMap(histpath);
  hist_leading_coincidence_vs_tdl->GetXaxis()->SetTitle("time (ns)");
  hist_leading_coincidence_vs_tdl->GetYaxis()->SetTitle("coincidence");

  // Subleading
  histpath = path + "/ODLS/SubleadingPulse/PulseArea";
  m_hists->BookFillHist(histpath, 1500., 0., 1500., s_pulseArea);
  TH1 *hist_subleading_phd = m_hists->GetHistFromMap(histpath);
  hist_subleading_phd->GetXaxis()->SetTitle("phd");

  histpath = path + "/ODLS/SubleadingPulse/PulseArea_vs_EventArea";
  m_hists->BookFillHist(histpath, 1500., 0., 1500., 1500., 0., 1500.,
                        g_ODEventArea, s_pulseArea);
  TH1 *hist_subleading_phd_vs_ea = m_hists->GetHistFromMap(histpath);
  hist_subleading_phd_vs_ea->GetXaxis()->SetTitle("event phd");
  hist_subleading_phd_vs_ea->GetYaxis()->SetTitle("subleading phd");

  histpath = path + "/ODLS/SubleadingPulse/PulseArea_vs_EventArea_difference";
  m_hists->BookFillHist(histpath, 1500., 0., 1500.,
                        g_ODEventArea - s_pulseArea);
  TH1 *hist_subleading_phd_vs_ea_diff = m_hists->GetHistFromMap(histpath);
  hist_subleading_phd_vs_ea_diff->GetXaxis()->SetTitle("phd difference");

  histpath = path + "/ODLS/SubleadingPulse/PulseArea_vs_EventArea_percentage";
  m_hists->BookFillHist(histpath, 200., 0., 100.,
                        (s_pulseArea / g_ODEventArea) * 100);
  TH1 *hist_subleading_phd_vs_ea_p = m_hists->GetHistFromMap(histpath);
  hist_subleading_phd_vs_ea_p->GetXaxis()->SetTitle("percentage");

  histpath = path + "/ODLS/SubleadingPulse/Coincidence";
  m_hists->BookFillHist(histpath, 120., 0., 120., s_coincidence);
  TH1 *hist_subleading_coincidence = m_hists->GetHistFromMap(histpath);
  hist_subleading_coincidence->GetXaxis()->SetTitle("subleading coincidence");

  histpath = path + "/ODLS/SubleadingPulse/H2W";
  m_hists->BookFillHist(histpath, 100., 0., 0.1, s_h2w);
  TH1 *hist_subleading_h2w = m_hists->GetHistFromMap(histpath);
  hist_subleading_h2w->GetXaxis()->SetTitle("h2w");

  histpath = path + "/ODLS/SubleadingPulse/PulseArea_vs_H2W";
  m_hists->BookFillHist(histpath, 100., 0., 0.1, 1500., 0., 1500., s_h2w,
                        s_pulseArea);
  TH1 *hist_subleading_phd_vs_h2w = m_hists->GetHistFromMap(histpath);
  hist_subleading_phd_vs_h2w->GetXaxis()->SetTitle("h2w");
  hist_subleading_phd_vs_h2w->GetYaxis()->SetTitle("phd");

  histpath = path + "/ODLS/SubleadingPulse/PulseArea_vs_TimeDifference_short";
  m_hists->BookFillHist(histpath, 600., -3000., 3000., 1500., 0., 1500.,
                        od_timediff, s_pulseArea);
  TH1 *hist_subleading_phd_vs_tds = m_hists->GetHistFromMap(histpath);
  hist_subleading_phd_vs_tds->GetXaxis()->SetTitle("time (ns)");
  hist_subleading_phd_vs_tds->GetYaxis()->SetTitle("phd");

  histpath = path + "/ODLS/SubleadingPulse/PulseArea_vs_TimeDifference_long";
  m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 1500., 0., 1500.,
                        od_timediff, s_pulseArea);
  TH1 *hist_subleading_phd_vs_tdl = m_hists->GetHistFromMap(histpath);
  hist_subleading_phd_vs_tdl->GetXaxis()->SetTitle("time (ns)");
  hist_subleading_phd_vs_tdl->GetYaxis()->SetTitle("phd");

  histpath = path + "/ODLS/SubleadingPulse/Coincidence_vs_TimeDifference_short";
  m_hists->BookFillHist(histpath, 600., -3000., 3000., 120., 0., 120.,
                        od_timediff, s_coincidence);
  TH1 *hist_subleading_coincidence_vs_tds = m_hists->GetHistFromMap(histpath);
  hist_subleading_coincidence_vs_tds->GetXaxis()->SetTitle("time (ns)");
  hist_subleading_coincidence_vs_tds->GetYaxis()->SetTitle("coincidence");

  histpath = path + "/ODLS/SubleadingPulse/Coincidence_vs_TimeDifference_long";
  m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., 120., 0., 120.,
                        od_timediff, s_coincidence);
  TH1 *hist_subleading_coincidence_vs_tdl = m_hists->GetHistFromMap(histpath);
  hist_subleading_coincidence_vs_tdl->GetXaxis()->SetTitle("time (ns)");
  hist_subleading_coincidence_vs_tdl->GetYaxis()->SetTitle("coincidence");

  // Leading vs Subleading
  histpath = path + "/ODLS/LeadingVsSubleading/PulseArea";
  m_hists->BookFillHist(histpath, 1500., 0., 1500., 1500., 0., 1500.,
                        l_pulseArea, s_pulseArea);
  TH1 *hist_pa2d = m_hists->GetHistFromMap(histpath);
  hist_pa2d->GetXaxis()->SetTitle("leading phd");
  hist_pa2d->GetYaxis()->SetTitle("subleading phd");

  histpath = path + "/ODLS/LeadingVsSubleading/PulseAreaRatio";
  m_hists->BookFillHist(histpath, 100., 0., 1., od_phdratio);
  TH1 *hist_par = m_hists->GetHistFromMap(histpath);
  hist_par->GetXaxis()->SetTitle("phd ratio");

  histpath = path + "/ODLS/LeadingVsSubleading/Coincidence";
  m_hists->BookFillHist(histpath, 120., 0., 120., 120., 0., 120., l_coincidence,
                        s_coincidence);
  TH1 *hist_c2d = m_hists->GetHistFromMap(histpath);
  hist_c2d->GetXaxis()->SetTitle("leading coincidence");
  hist_c2d->GetYaxis()->SetTitle("subleading coincidence");

  histpath = path + "/ODLS/LeadingVsSubleading/CoincidenceRatio";
  m_hists->BookFillHist(histpath, 100., 0., 1., od_coincidenceratio);
  TH1 *hist_cr = m_hists->GetHistFromMap(histpath);
  hist_cr->GetXaxis()->SetTitle("coincidence ratio");

  histpath = path + "/ODLS/LeadingVsSubleading/TimeDifference_short";
  m_hists->BookFillHist(histpath, 600., -3000., 3000., od_timediff);
  TH1 *hist_tds = m_hists->GetHistFromMap(histpath);
  hist_tds->GetXaxis()->SetTitle("time (ns)");

  histpath = path + "/ODLS/LeadingVsSubleading/TimeDifference_long";
  m_hists->BookFillHist(histpath, 20000., -1000000., 1000000., od_timediff);
  TH1 *hist_tdl = m_hists->GetHistFromMap(histpath);
  hist_tdl->GetXaxis()->SetTitle("time (ns)");

  histpath = path + "/ODLS/LeadingVsSubleading/H2W";
  m_hists->BookFillHist(histpath, 100., 0., 0.1, 100., 0., 0.1, l_h2w, s_h2w);
  TH1 *hist_h2w_2d = m_hists->GetHistFromMap(histpath);
  hist_h2w_2d->GetXaxis()->SetTitle("leading h2w");
  hist_h2w_2d->GetYaxis()->SetTitle("subleading h2w");

  histpath = path + "/ODLS/LeadingVsSubleading/H2W_Vs_Area_ratios";
  m_hists->BookFillHist(histpath, 200., 0., 2., 200., 0., 2., od_phdratio,
                        od_h2wratio);
  TH1 *hist_h2w_phd_r = m_hists->GetHistFromMap(histpath);
  hist_h2w_phd_r->GetYaxis()->SetTitle("h2w ratio");
  hist_h2w_phd_r->GetXaxis()->SetTitle("phd ratio");

  histpath = path + "/ODLS/LeadingVsSubleading/Mulitiplicity";
  m_hists->BookFillHist(histpath, 3000., 0., 3000., pulseMultiplicity);
  TH1 *hist_multiplicity = m_hists->GetHistFromMap(histpath);
  hist_multiplicity->GetYaxis()->SetTitle("n. pulses");

  // Select Regions
  if (selectRegion) {
    std::string region;
    // Selected from leading phd vs OD event phd
    region = "/RegionSelection/LeadingArea_Vs_EventArea/";
    // Case 1
    if (l_pulseArea / g_ODEventArea > 0.80) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case1", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_EventArea_Case1", pulseIDs);
    }
    // Case 2
    if (l_pulseArea / g_ODEventArea > 0.70) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case2", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_EventArea_Case2", pulseIDs);
    }
    // Case 3
    if (l_pulseArea / g_ODEventArea > 0.50 &&
        l_pulseArea / g_ODEventArea < 0.80) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case3", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_EventArea_Case3", pulseIDs);
    }
    // Case 4
    if (l_pulseArea / g_ODEventArea > 0.30 &&
        l_pulseArea / g_ODEventArea < 0.45) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case4", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_EventArea_Case4", pulseIDs);
    }
    // Case 5
    if (l_pulseArea / g_ODEventArea > 0.10 &&
        l_pulseArea / g_ODEventArea < 0.30) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case5", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_EventArea_Case5", pulseIDs);
    }
    // Case 6
    if (l_pulseArea / g_ODEventArea < 0.10) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case6", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_EventArea_Case6", pulseIDs);
    }
    // Selection by Leading Vs Subleading Area
    region = "/RegionSelection/LeadingArea_Vs_SubleadingArea/";
    // Case 1
    if (l_pulseArea < 20 && s_pulseArea < 20) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case1", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_SubleadingArea_Case1",
                pulseIDs);
    }
    // Case 2
    if (l_pulseArea > 100 && l_pulseArea < 300) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case2", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_SubleadingArea_Case2",
                pulseIDs);
    }
    // Case 3
    if (l_pulseArea > 300) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case3", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent("RegionSelection_LeadingArea_Vs_SubleadingArea_Case3",
                pulseIDs);
    }
    // Selection by Leading Vs Subleading Coincidence
    region = "/RegionSelection/LeadingCoincidence_Vs_SubleadingCoincidence/";
    // Case 1
    if (l_coincidence > 110 && s_coincidence > 40) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case1", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent(
          "RegionSelection_LeadingCoincidence_Vs_SubleadingCoincidence_Case1",
          pulseIDs);
    }
    // Case 2
    if (l_coincidence > 60 && l_coincidence < 100 && s_coincidence > 40 &&
        s_coincidence < 80) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case2", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent(
          "RegionSelection_LeadingCoincidence_Vs_SubleadingCoincidence_Case2",
          pulseIDs);
    }
    // Case 3
    if (l_coincidence > 60 && l_coincidence < 100 && s_coincidence < 30) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case3", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent(
          "RegionSelection_LeadingCoincidence_Vs_SubleadingCoincidence_Case3",
          pulseIDs);
    }
    // Case 4
    if (l_coincidence < 30 && s_coincidence < 20) {
      ODLeadingVsSubLeadingHistograms(path + region + "Case4", pulseIDs,
                                      pulseMultiplicity, false);
      SaveEvent(
          "RegionSelection_LeadingCoincidence_Vs_SubleadingCoincidence_Case4",
          pulseIDs);
    }
    // If 300ns
    if (od_timediff > 0 && od_timediff < 400) {
      ODLeadingVsSubLeadingHistograms(
          path + "/RegionSelection/TimeDifference300ns", pulseIDs,
          pulseMultiplicity, false);
    }
  }
}

void VetoDev::SaveEvent(std::string outName, std::vector<int> pulseIDs) {

  ofstream outfile1;
  std::string outName1 = "/global/homes/s/seriksen/Veto/VetoDev/run/VetoDev/" +
                         outName + "_forLZAP.txt";
  outfile1.open(outName1, std::ios_base::app);
  outfile1 << (*m_event->m_eventHeader)->rawFileName << " "
           << (*m_event->m_eventHeader)->runID << " "
           << (*m_event->m_eventHeader)->eventID << "\n";
  outfile1.close();

  ofstream outfile2;
  std::string outName2 = "/global/homes/s/seriksen/Veto/VetoDev/run/VetoDev/" +
                         outName + "_forME.txt";
  outfile2.open(outName2, std::ios_base::app);
  outfile2 << (*m_event->m_eventHeader)->rawFileName << ","
           << (*m_event->m_eventHeader)->runID << ","
           << (*m_event->m_eventHeader)->eventID;

  for (auto &&id : pulseIDs) {
    outfile2 << "," << id << ","
             << (*m_event->m_odHGPulses)->pulseStartTime_ns[id];
  }
  outfile2 << "\n";
  outfile2.close();
}

void VetoDev::ODPeakSave(int p_id) {

  ofstream outfile;
  outfile.open(
      "/global/homes/s/seriksen/Veto/VetoDev/run/VetoDev/300nspeak_forLZAP.txt",
      std::ios_base::app);
  outfile << "/global/cfs/cdirs/lz/data/MDC3/calibration/"
             "BACCARAT-4.11.0_DER-8.5.15/20180227/"
          << (*m_event->m_eventHeader)->rawFileName << " "
          << (*m_event->m_eventHeader)->runID << " "
          << (*m_event->m_eventHeader)->eventID << "\n";
  outfile.close();

  ofstream outfile1;
  outfile1.open(
      "/global/homes/s/seriksen/Veto/VetoDev/run/VetoDev/300nspeak_forME.txt",
      std::ios_base::app);
  outfile1 << (*m_event->m_eventHeader)->rawFileName << ","
           << (*m_event->m_eventHeader)->runID << ","
           << (*m_event->m_eventHeader)->eventID << "," << p_id << ","
           << (*m_event->m_odHGPulses)->pulseStartTime_ns[p_id] << "\n";
  outfile1.close();
}