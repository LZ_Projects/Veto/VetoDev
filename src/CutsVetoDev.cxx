#include "CutsVetoDev.h"
#include "ConfigSvc.h"

CutsVetoDev::CutsVetoDev(EventBase *eventBase) { m_event = eventBase; }

CutsVetoDev::~CutsVetoDev() {}

// Function that lists all of the common cuts for this Analysis
bool CutsVetoDev::VetoDevCutsOK() {
  // List of common cuts for this analysis into one cut
  return true;
}

bool CutsVetoDev::ODPulseCut(int coincidence, float phd, int cutlevel) {
  bool cutResult{false};
  float cut_0{ConfigSvc::Instance()->configFileVarMap["odPulseCut"]};
  int cut_1{ConfigSvc::Instance()->configFileVarMap["odPulseCut_coincidence"]};
  float cut_2{ConfigSvc::Instance()->configFileVarMap["odPulseCut_phd"]};
  if (cutlevel == 0) {
    if (phd / coincidence > cut_0)
      cutResult = true;
  } else if (cutlevel == 1) {
    if (phd / coincidence > cut_0 && coincidence > cut_1)
      cutResult = true;
  } else if (cutlevel == 2) {
    if (phd / coincidence > cut_0 && coincidence > cut_1 && phd > cut_2)
      cutResult = true;
  } else if (cutlevel == 3) {
    if (coincidence > cut_1 && phd > cut_2)
      cutResult = true;
  }
  return cutResult;
}

bool CutsVetoDev::WIMP_ROI_Veto(int s1ID) {
  float s1Coincidence{ConfigSvc::Instance()->configFileVarMap["s1Coincidence"]};
  float s1AreaMax{ConfigSvc::Instance()->configFileVarMap["s1AreaMax"]};
  float s2AreaMin{ConfigSvc::Instance()->configFileVarMap["s2AreaMin"]};
  float s2AreaMax{ConfigSvc::Instance()->configFileVarMap["s2AreaMax"]};
  float s2RawAreaMin{ConfigSvc::Instance()->configFileVarMap["s2RawAreaMin"]};

  if ((*m_event->m_tpcPulses)->coincidence[s1ID] >= s1Coincidence &&
      (*m_event->m_singleScatter)->correctedS1Area_phd < s1AreaMax &&
      (*m_event->m_singleScatter)->correctedS2Area_phd > s2AreaMin &&
      (*m_event->m_singleScatter)->correctedS2Area_phd < s2AreaMax &&
      (*m_event->m_singleScatter)->s2Area_phd > s2RawAreaMin) {
    return true;
  }
  return false;
}

bool CutsVetoDev::UncorrectedFiducialVeto() {
  double radius{sqrt(pow((*m_event->m_singleScatter)->x_cm, 2) +
                     pow((*m_event->m_singleScatter)->y_cm, 2))};
  double driftTime_us{(*m_event->m_singleScatter)->driftTime_ns / 1000.};

  if ((driftTime_us < m_idealFiducialLowerBoundary_us) &&
      (driftTime_us > m_idealFiducialUpperBoundary_us)) {

    double boundaryR_cm{0};
    for (size_t i = 0; i < m_idealFiducialWallFit.size(); i++) {
      boundaryR_cm +=
          .1 * m_idealFiducialWallFit.at(i) * pow(driftTime_us / 1000.0, i);
    }
    // Return true if in fiducial volume
    return (radius < boundaryR_cm);
  }
  return false;
}

bool CutsVetoDev::SkinVeto(int s1ID) {

  float skinCoincidence{
      ConfigSvc::Instance()->configFileVarMap["skinCoincidence"]};
  float skinThreshold{ConfigSvc::Instance()->configFileVarMap["skinPulseArea"]};
  int deltaT_ns{0};

  int s1Time{(*m_event->m_tpcPulses)->pulseStartTime_ns[s1ID] +
             (*m_event->m_tpcPulses)->areaFractionTime1_ns[s1ID]};
  // Loop over Skin pulses
  for (int p = 0; p < (*m_event->m_skinPulses)->nPulses; p++) {
    deltaT_ns = TMath::Abs(((*m_event->m_skinPulses)->pulseStartTime_ns[p] +
                            (*m_event->m_skinPulses)->areaFractionTime1_ns[p]) -
                           s1Time);

    if ((*m_event->m_skinPulses)->coincidence[p] >= skinCoincidence &&
        (*m_event->m_skinPulses)->pulseArea_phd[p] > skinThreshold &&
        deltaT_ns < 500000) {
      return false;
    }
  }
  return true;
}

bool CutsVetoDev::ODVeto(int s1ID, std::vector<int> odPulses) {

  int s1Time{(*m_event->m_tpcPulses)->pulseStartTime_ns[s1ID] +
             (*m_event->m_tpcPulses)->areaFractionTime1_ns[s1ID]};
  float cut_coincidence{
      ConfigSvc::Instance()->configFileVarMap["odCoincidence"]};
  float cut_phd{ConfigSvc::Instance()->configFileVarMap["odPulseArea"]};
  int deltaT_ns;
  std::vector<int>::iterator it;
  float coincidence;
  float phd;
  // Loop over OD Pulses
  for (auto &&id : odPulses) {
    deltaT_ns =
        TMath::Abs(((*m_event->m_odHGPulses)->pulseStartTime_ns[id] +
                    (*m_event->m_odHGPulses)->areaFractionTime1_ns[id]) -
                   s1Time);
    coincidence = (*m_event->m_odHGPulses)->coincidence[id];
    phd = (*m_event->m_odHGPulses)->pulseArea_phd[id];
    if (coincidence >= cut_coincidence && phd > cut_phd && deltaT_ns < 500000) {
      return false;
    }
  }
  return true;
}