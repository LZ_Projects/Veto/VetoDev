#ifndef VetoDev_H
#define VetoDev_H

#include "Analysis.h"

#include "CutsVetoDev.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class VetoDev : public Analysis {

public:
  VetoDev();
  ~VetoDev();

  void Initialize();
  void Execute();
  void Finalize();

  /**
   * @brief Control when Histogram functions are called and run them with
   * various combinations of PMTs being turned off
   * @param path save location in ROOT file
   * @param pulseIDs od pulse IDs
   * @param processSS if true call SS comparison histogram creation
   */
  void ODProcessODPulses(std::string path, std::vector<int> pulseIDs,
                         bool processSS);

  /**
   * @brief Apply MDC3 Cuts and call ODProcessODPulses
   * Cut order;
   * 0. SS
   * 1. WIMP ROI
   * 2. Fiducial
   * 3. Skin
   * 4. OD
   * @param path save location in ROOT file
   * @param odpulseIDs od pulse IDs
   */
  void ApplyMDC3Cuts(std::string path, std::vector<int> odpulseIDs);

  /**
   * \brief Loop over all pulses in event and get pulse ID
   * \return Vector of indices of the pulses
   */
  std::vector<int> ODGetAllPulseIDs();

  /**
   * @brief Loop over vector of pulse IDs. Filter pulses which don't pass a cut
   * @param odPulses pulse IDs
   * @param cutlevel cut level, see CutsVetoDev::ODPulseCut
   * @return surviving pulse IDs
   */
  std::vector<int> ODGetCutPulseIDs(std::vector<int> odPulses, int cutlevel);

  /**
   * @brief Get pulse ID of largest pulse (by phd) from vector of pulse IDs
   * @param odPulses pulse IDs
   * @return largest pulseID (as vector so don't need to adapt rest of code)
   */
  std::vector<int> ODGetLeadingPulseID(std::vector<int> odPulses);

  /**
   * @brief Get pulse IDs of 3 largest pulses (by phd) from vector of pulse IDs.
   * Requires odPulses.size() >= 2 otherwise returned vector is empty.
   * @param odPulses pulse IDs
   * @return vector of size 2 containing pulseIDs (largest to smallest) or an
   * empty vector
   */
  std::vector<int> ODGetLeadingAndSubLeadingPulses(std::vector<int> odPulses);

  /**
   * @brief Calculate the event pulse area
   * @param pulseIDs pulse IDs (should be all OD pulse IDs)
   * @return sum of phd seen by pulses in pulseIDs vector
   */
  float ODGetEventPhd(std::vector<int> pulseIDs);

  /**
   * \brief Fill standard set of OD Histograms.
   * These RQs feed into HADES for pulse classification.
   * Histogram List;
   * 1. Coincidence (1D)
   * 2. Pulse Area (1D)
   * 3. Area Fraction Time 5% (1D)
   * 4. Area Fraction Time 95% (1D)
   * 5. Pulse Width 90 (95% - 5% area fraction time) (1D)
   * 6. Pulse Width RMS (1D)
   * 7. Pulse Height (1D)
   * 8. Pulse Rise Time (1D)
   * 9. Pulse Fraction 50ns (1D)
   * 10. Pulse Fraction 200ns (1D)
   * 11. Pulses Per Channel (1D)
   * \param path save location in ROOT file
   * \param pulseIDs
   */
  void ODPulseClassificationHistograms(std::string path,
                                       std::vector<int> pulseIDs);

  /**
   * @brief Fill a set of Histograms with OD information and SS information
   * The Time Difference is trigger time + 5%, OD - S1
   * Histogram List;
   * 1. Time Difference, coarse binning (1D)
   * 2. Time Difference, fine short (1D)
   * 3. S1 phd vs Time Difference, coarse binning (2D)
   * 4. S1 phd vs Time Difference, fine binning (2D)
   * 5. od phd / amp vs Time Difference, coarse binning (2D)
   * 6. od phd / amp vs Time Difference, fine binning (2D)
   * 7. od phd vs Time Difference, coarse binning (2D)
   * 8. od phd vs Time Difference, fine binning (2D)
   * 9. od coincidence vs Time Difference, coarse binning (2D)
   * 10. od coincidence vs Time Difference, fine binning (2D)
   * 11. od coincidence/phd vs Time Difference, fine binning (2D)
   * 12. od coincidence/phd vs Time Difference, coarse binning (2D)
   * @param path save location in ROOT file
   * @param pulseIDs OD pulse IDs
   */
  void ODPulsesRelativeToSS(std::string path, std::vector<int> pulseIDs);

  /**
   * @brief Control of Leading vs Subleading OD Pulse Study.
   * Calculating leading vs subleading and call histogram function.
   * @param histpath ROOT file save location
   * @param pulseIDs od pulse IDs to perform analysis on
   */
  void ODLeadingVsSubleadingStudy(std::string histpath,
                                  std::vector<int> pulseIDs);

  /**
   * @brief Create histograms for leading and subleading OD pulses in event.
   * 3 Histogram List; Leading, Subleading, Combination.
   * If SaveEvent, if pulses lie in set phase space, isolate and recall this
   * function.
   * Leading;
   * 1. Pulse Area (1D)
   * 2. Pulse Area vs Event Area (2D)
   * 3. Pulse Area vs Event Area Difference (1D)
   * 4. Pulse Area vs Event Area Percentage (1D)
   * 5. Coincidence (1D)
   * 6. H2W (1D)
   * 7. phd vs h2w (2D)
   * 8. Pulse Area vs Time Difference - fine binning (2D)
   * 9. Pulse Area vs Time Difference - coarse binning (2D)
   * 10. Coincidence vs Time Difference - fine binning (2D)
   * 11. Coincidence vs Time Difference - coarse binning (2D)
   * Subleading;
   * 1. Pulse Area (1D)
   * 2. Pulse Area vs Event Area (2D)
   * 3. Pulse Area vs Event Area Difference (1D)
   * 4. Pulse Area vs Event Area percentage (1D)
   * 5. Coincidence (1D)
   * 6. H2W (1D)
   * 7. phd vs h2w (2D)
   * 8. Pulse Area vs Time Difference - fine binning (2D)
   * 9. Pulse Area vs Time Difference - coarse binning (2D)
   * 10. Coincidence vs Time Difference - fine binning (2D)
   * 11. Coincidence vs Time Difference - coarse binning (2D)
   * Combination;
   * 1. Pulse Area (2D)
   * 2. Pulse Area Ratio (1D)
   * 3. Coincidence (2D)
   * 4. Coincidence Ratio (1D)
   * 5. Time difference, fine binning (1D)
   * 6. Time difference, coarse binning (1D)
   * 7. H2W (2D)
   * 8. H2W vs phd ratio (2D)
   * @param path save location in ROOT file
   * @param pulseIDs OD pulse IDs [0] = leading [1] = subleading
   * @param pulseMultiplicity number of pulses in event surviving the cut
   * @param saveRegion if true calls SaveEvent
   */
  void ODLeadingVsSubLeadingHistograms(std::string path,
                                       std::vector<int> pulseIDs,
                                       int pulseMultiplicity,
                                       bool saveRegion);

  /**
   * @brief Save the event for processing in LZap
   * @param outName outname for file
   * @param pulseIDs pulses to save to navigate LZap waveform viewer easier
   */
  void SaveEvent(std::string outName, std::vector<int> pulseIDs);

  /**
   * @brief Save 300ns peak
   * @param p_id
   */
  void ODPeakSave(int p_id);

protected:
  CutsVetoDev *m_cutsVetoDev;
  ConfigSvc *m_conf;

  /// Sum of phd seen by all pulses in the OD for the event
  float g_ODEventArea;
  // Dead PMT vectors
  /*
  const std::vector<int> allPMTsOn{-1};
  const std::vector<int> OnePMTDead{800};
  const std::vector<int> LadderPMTDead{800, 820, 840, 860, 880, 900};
  const std::vector<int> SquarePMTDead{830, 831, 832, 850, 851, 852};
  const std::vector<int> OneRowPMTDead{820, 821, 822, 823, 824, 825, 826, 827,
  828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839}; const
  std::vector<int> TwoRowsPMTDead{800, 801, 802, 803, 804, 805, 806, 807, 808,
  809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823,
  824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838, 839
  };
   */
};

#endif
