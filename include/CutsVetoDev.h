#ifndef CutsVetoDev_H
#define CutsVetoDev_H

#include "EventBase.h"

class CutsVetoDev {

public:
  CutsVetoDev(EventBase *eventBase);
  ~CutsVetoDev();
  bool VetoDevCutsOK();

  /**
   * @brief Determine if pulse survives phd / coincidence cut
   * @param coincidence pulse coincidence
   * @param phd pulse area
   * @param cutlevel select which cut to perform
   * @return true if phd / coincidence > cut, else false
   */
  bool ODPulseCut(int coincidence, float phd, int cutlevel);

  /**
   * @brief Determine if SS event is in WIMP region of interest
   * @param id s1 pulseID
   * @return true is in ROI, else false
   */
  bool WIMP_ROI_Veto(int id);

  /**
   * @brief Determine if SS event is inside fiducial volume
   * @return true if in volume, else false
   */
  bool UncorrectedFiducialVeto();

  /**
   * @brief Determine if there is a significant event in the Skin detector
   * To count as significant Skin event must pass all three of the following;
   * 1. skin coincidence >= threshold
   * 2. skin phd > threshold
   * 3. skin pulse start time - s1 time < 500000 ns
   * All Skin pulses are checked
   * @param id s1 pulseID
   * @return true is no significant Skin pulse, else false
   */
  bool SkinVeto(int id);

  /**
   * @brief Determine if there is a significant event in the OD detector
   * To count as significant OD event must pass all three of the following;
   * 1. od coincidence >= threshold
   * 2. od phd > threshold
   * 3. od pulse time - s1 time < 500000 ns
   * Only OD pulses passed as a vector are checked
   * @param s1id s1 pulseID
   * @param odpulseIDs od pulseIDs
   * @return true is no significant pulse, else false
   */
  bool ODVeto(int s1id, std::vector<int> odpulseIDs);

  // TODO Add etrain

private:
  EventBase *m_event;
  /// Fiducial Volume Boundaries in x and y
  static constexpr std::array<double, 10> m_idealFiducialWallFit = {
      6.92312336e+02, -2.43739552e+02, 2.84329329e+03, -2.09128802e+04,
      9.67156033e+04, -2.84628041e+05, 5.31021633e+05, -6.06995609e+05,
      3.87230616e+05, -1.05486503e+05};
  /// Fiducial Volume lower boundary in seconds for z
  static constexpr double m_idealFiducialLowerBoundary_us{788.629};
  /// Fiducial volume upper boundary in seconds for z
  static constexpr double m_idealFiducialUpperBoundary_us{72.414};
};

#endif
