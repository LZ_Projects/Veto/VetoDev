#!/bin/bash
VetoDevALPACA_DIR="${VetoDevALPACA_DIR:-/global/homes/s/seriksen/Veto/VetoDev}"
VetoDev_NCores="${VetoDev_NCores:-12}"
cd $VetoDevALPACA_DIR
for (( DD=1; DD<=9; DD++ ))
do
    python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs/lz/data/MDC3/background/LZAP-4.7.0/2018040${DD}/*lzap.root -o VetoDev_WIMP_0${DD} -n ${VetoDev_NCores} -m VetoDev
done
for (( DD=10; DD<=30; DD++ ))
do
    python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs/lz/data/MDC3/background/LZAP-4.7.0/201804${DD}/*lzap.root -o VetoDev_WIMP_${DD} -n ${VetoDev_NCores} -m VetoDev
done
 hadd
cd run/VetoDev
hadd VetoDev_wimpfull.root VetoDev_WIMP_*
rm VetoDev_WIMP*
mv VetoDev_wimpfull.root VetoDev_WIMP_30days_LZAP470_April.root

#cd $VetoDevALPACA_DIR
#for (( DD=1; DD<=9; DD++ ))
#do
#    python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs/lz/data/MDC3/background/LZAP-4.8.3/2018050${DD}/*lzap.root -o VetoDev_WIMP_0${DD} -n ${VetoDev_NCores} -m VetoDev
#done
#for (( DD=10; DD<=31; DD++ ))
#do
#    python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs/lz/data/MDC3/background/LZAP-4.8.3/201805${DD}/*lzap.root -o VetoDev_WIMP_${DD} -n ${VetoDev_NCores} -m VetoDev
#done
# hadd
#cd run/VetoDev
#hadd VetoDev_wimpfull.root VetoDev_WIMP_*
#rm VetoDev_WIMP*
#mv VetoDev_wimpfull.root VetoDev_WIMP_30days_LZAP483_April.root