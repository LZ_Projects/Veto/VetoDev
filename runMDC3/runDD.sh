#!/bin/bash
VetoDevALPACA_DIR="${VetoDevALPACA_DIR:-/global/homes/s/seriksen/Veto/VetoDev}"
VetoDev_NCores="${VetoDev_NCores:-12}"
cd $VetoDevALPACA_DIR
for (( DD=26; DD<=28; DD++ ))
do
    python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs/lz/data/MDC3/calibration/LZAP-4.7.0/201802${DD}/*lzap.root -o VetoDev_DD_${DD} -n ${VetoDev_NCores} -m VetoDev
done
# hadd
cd run/VetoDev
hadd VetoDev_DD_12hours.root VetoDev_DD_*
rm VetoDev_DD_2*