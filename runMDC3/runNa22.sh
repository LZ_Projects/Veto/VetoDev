#!/bin/bash
VetoDevALPACA_DIR="${VetoDevALPACA_DIR:-/global/homes/s/seriksen/Veto/VetoDev}"
VetoDev_NCores="${VetoDev_NCores:-12}"
cd $VetoDevALPACA_DIR
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180212/lz_2018021200*.root -o VetoDev_Na22_1 -n ${VetoDev_NCores} -m VetoDev
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180212/lz_2018021203*.root -o VetoDev_Na22_2 -n ${VetoDev_NCores} -m VetoDev
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180212/lz_2018021205*.root -o VetoDev_Na22_3 -n ${VetoDev_NCores} -m VetoDev
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180212/lz_2018021208*.root -o VetoDev_Na22_4 -n ${VetoDev_NCores} -m VetoDev
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180212/lz_2018021206*.root -o VetoDev_Na22_5 -n ${VetoDev_NCores} -m VetoDev
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180213/lz_2018021300*.root -o VetoDev_Na22_6 -n ${VetoDev_NCores} -m VetoDev
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180213/lz_2018021303*.root -o VetoDev_Na22_7 -n ${VetoDev_NCores} -m VetoDev
python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs//lz/data/MDC3/calibration/LZAP-4.7.0/20180213/lz_2018021306*.root -o VetoDev_Na22_8 -n ${VetoDev_NCores} -m VetoDev
# hadd
cd run/VetoDev
hadd VetoDev_Na22.root VetoDev_Na22_*
rm VetoDev_Na22_*