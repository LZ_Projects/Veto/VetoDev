export VetoDev_Prime=$(pwd)
export VetoDevALPACA_DIR=/global/homes/s/seriksen/Veto/VetoDev
export VetoDev_NCores=12

cd $VetoDev_Prime
source runDD.sh
cd $VetoDev_Prime
source runWIMP.sh
cd $VetoDev_Prime
source runNa22.sh
cd $VetoDev_Prime
source runGd152.sh
