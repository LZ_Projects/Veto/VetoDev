#!/bin/bash
VetoDevALPACA_DIR="${VetoDevALPACA_DIR:-/global/homes/s/seriksen/Veto/VetoDev}"
VetoDev_NCores="${VetoDev_NCores:-12}"
cd $VetoDevALPACA_DIR
#
python analysisScripts/RunNJobs_localDataset.py -d /global/project/projectdirs/lz/users/seriksen/ODDeadPMTStudy/Gd/AllOn/lzap/*lzap.root -o VetoDev_Gd -n ${VetoDev_NCores} -m VetoDev
