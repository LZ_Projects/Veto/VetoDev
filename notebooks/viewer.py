import ROOT
import copy
import math
import operator
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import uproot

def get_hist(rfile, histname):
    hist = rfile.Get(histname)
    return copy.deepcopy(hist)

def get_keynames(self, dir=""):
    self.cd(dir)
    return [key.GetName() for key in ROOT.gDirectory.GetListOfKeys()]

def format_hist(hist, i):
    """Set histogram colour, axis title offset, etc...
    TODO: Move logging and scales to here
    """
    hist.SetMarkerStyle(7)
    hist.SetMarkerColor(colors[i] + 2)
    hist.SetLineColor(colors[i] + 2)
    hist.SetLineWidth(2)
    hist.SetMarkerColorAlpha(colors[i] + 2, 0.4)
    hist.GetXaxis().SetTitleOffset(1.5);
    hist.GetYaxis().SetTitleOffset(1.5);


def addHistogramToCanvas(canvas, stack, hist, **kwargs):
    """TODO
    """

    print('to be made')

def fillCanvas(canvas, stack, fname, histName, i, j, norm, logs, scales, drawtype, haslegend, legendname, fitgauss, normconst, autolimit):
    """Add a single histogram to the canvas
    """
    rfile = ROOT.TFile(fname)
    canvas.cd(i + 1)
    hist = get_hist(rfile, histName)
    try:
        if norm:
            nfactor = hist.Integral() / normconst # average seen by each PMT
            hist.Scale(1/nfactor)
        if autolimit:
            max_x_bin = hist.FindLastBinAbove(0,1)
            max_x = hist.GetBinCenter(max_x_bin)
            axis = hist.GetXaxis()
            axis.SetRangeUser(0, max_x)
        else:
            if scales[0]:
                axis = hist.GetXaxis()
                axis.SetRangeUser(scales[0][0], scales[0][1])
            if scales[1]:
                axis = hist.GetYaxis()
                axis.SetRangeUser(scales[1][0], scales[1][1])
            if scales[2]:
                axis = hist.GetZaxis()
                axis.SetRangeUser(scales[2][0], scales[2][1])
        shortname = fname[fname.rfind('/')+1:fname.find('.')]
        histTitle = shortname + '/' + histName
        format_hist(hist, j)
        hist.SetTitle(histTitle)
        if logs[0]:
            canvas.cd(i+1).SetLogx()
        if logs[1]:
            canvas.cd(i+1).SetLogy()
        if logs[2]:
            canvas.cd(i+1).SetLogz()
        stack.Add(hist)
        if fitgauss:
            hist.Fit('gaus', 'S')
        hist.Draw(drawtype + ' same')
        if haslegend:
            legend.AddEntry(hist,legendname,'l')
            legend.Draw('same')
        return canvas, stack
    except:
        print('Unable to add hist', histName, 'to canvas')
        return canvas, stack

def setupVariables(scales):

    print('TODO')


def setupCanvas(fileWithHists, histList,
                ncols=1, nOnCanvas =1,
                logs=[False,False,False], norm=False,
                scales=[[],[],[]], drawtype='colz',
                haslegend=False, legendname=None,
                infolevel='none', fitgauss=False,
                normconst = 1, autolimit=False):
    """TODO: turn options into kwargs**
    TODO: turn into a class
    TODO: handle cases where fileWithHists is not a list and isn't same size as histList
    TODO: change so that histList is [[all hist1],[all hist2]] so can remove nOnCanvas and have multiples
    """

    nrows = int(math.ceil(((len(histList))/nOnCanvas)/ncols))
    canvas = ROOT.TCanvas("c","c",ncols*500, nrows*500)
    canvas.Divide(ncols,nrows)
    stack = ROOT.THStack()
    legend.Clear()

    if infolevel == 'none':
        ROOT.gStyle.SetOptStat(000000)
    elif infolevel == 'all':
        ROOT.gStyle.SetOptStat(111111)

    for i in range(int(len(histList)/nOnCanvas)):
        legend.Clear()
        for j in range(nOnCanvas):
            thisfile = fileWithHists[j::nOnCanvas][i]
            thishist = histList[j::nOnCanvas][i]
            if legendname is None:
                lname = thishist[(thishist.rfind('/'))::]
            else:
                lname = legendname[j::nOnCanvas][i]
            canvas, stack = fillCanvas(canvas, stack,
                                       thisfile, thishist,
                                       i, j, norm, logs, scales,
                                       drawtype, haslegend, lname,
                                       fitgauss, normconst, autolimit)

    return canvas

def histDifference(file1, file2, histname1, histname2, norm=False, diffType='diff', normfactor=1, dim='1D', scales=[[],[],[]], logs=[False,False,False]):
    """Calculate the difference between two histograms.
    """

    canvas = ROOT.TCanvas("c","c",1000, 500)
    canvas.Divide(1,1)
    canvas.cd(1)
    stack = ROOT.THStack()

    rfile = ROOT.TFile(file1); hist1 = get_hist(rfile, histname1)
    rfile = ROOT.TFile(file2); hist2 = get_hist(rfile, histname2)

    hist1.Divide(hist2)

    if scales[0]:
        axis = hist1.GetXaxis(); axis.SetRangeUser(scales[0][0], scales[0][1])
    if scales[1]:
        axis = hist1.GetYaxis(); axis.SetRangeUser(scales[1][0], scales[1][1])
    if scales[2]:
        axis = hist1.GetZaxis(); axis.SetRangeUser(scales[2][0], scales[2][1])
    if logs[0]:
        canvas.cd(1).SetLogx()
    if logs[1]:
        canvas.cd(1).SetLogy()
    if logs[2]:
        canvas.cd(1).SetLogz()
    format_hist(hist1, 0)
    stack.Add(hist1)
    hist1.Draw(drawtype)

    return canvas

def getFileHists(fname, displayinfo=False, key=None):
    """Quickest to just use uproot
    TODO: change to split into directories; uproot.allitems
    """

    rfile = uproot.open(fname)
    histList = rfile.allkeys()

    if displayinfo:
        for hist in histList:
            print(hist)
    if key:
        # Find all with a key
        keyhist = []
        for hist in histList:
            if key in str(hist):
                keyhist.append(hist)
        return keyhist
    return histList


legend = ROOT.TLegend(0.6,0.6,0.89,0.89)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetFillColor(0)
legend.SetTextSize(0.03)

ROOT.gStyle.SetPalette(ROOT.kRainBow)
colors=[ROOT.kBlue, ROOT.kRed, ROOT.kGreen, ROOT.kOrange, ROOT.kMagenta, ROOT.kCyan, ROOT.kViolet, ROOT.kPink, ROOT.kSpring, ROOT.kTeal]
ROOT.TFile.GetKeyNames = get_keynames